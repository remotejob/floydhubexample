import torch
from torch import nn
from torch.utils.data import Dataset
import numpy as np


class FinBotDataset(Dataset):
    def __init__(self, data_root, maxlen):
        self.data_root = data_root
        self.samples = []
        self.maxlen = maxlen
        self._init_dataset()
        self.chars = self.chars()
        self.int2char = self.int2char()
        self.char2int = self.char2int()
        self.input_seq = []
        self.target_seq = []
        self._init_seq()

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        # phrase = self.samples[idx]
        
        return self.input_seq[idx], self.target_seq[idx]

    def _init_dataset(self):

        with open(self.data_root) as fp:
            line = fp.readline()
            while line:
                line = fp.readline()
                self.samples.append(line)
        for i in range(len(self.samples)):
            while len(self.samples[i]) < self.maxlen:
                self.samples[i] += ' '

    def chars(self):
        return set(''.join(self.samples))

    def int2char(self):
        return dict(enumerate(self.chars))

    def char2int(self):
        return {char: ind for ind, char in self.int2char.items()}

    def one_hot_encode(self,sequence, dict_size, seq_len, batch_size):
        # Creating a multi-dimensional array of zeros with the desired output shape
        features = np.zeros((batch_size, seq_len, dict_size), dtype=np.float32)

        # Replacing the 0 at the relevant character index with a 1 to represent that character
        for i in range(batch_size):
            for u in range(seq_len):
                features[i, u, sequence[i][u]] = 1
        return features

    def _init_seq(self):
        for i in range(len(self.samples)):
            # Remove last character for input sequence
            self.input_seq.append(self.samples[i][:-1])

            # Remove firsts character for target sequence
            self.target_seq.append(self.samples[i][1:])

        for i in range(len(self.samples)):
            self.input_seq[i] = [self.char2int[character]
                                 for character in self.input_seq[i]]
            self.target_seq[i] = [self.char2int[character]
                                  for character in self.target_seq[i]]

        dict_size = len(self.char2int)
        seq_len = self.maxlen - 1
        batch_size = len(self.samples)

        self.input_seq = self.one_hot_encode(self.input_seq, dict_size, seq_len, batch_size)
        #print("Input shape: {} --> (Batch Size, Sequence Length, One-Hot Encoding Size)".format(input_seq.shape))

        self.input_seq = torch.from_numpy(self.input_seq)
        self.target_seq = torch.Tensor(self.target_seq)
    # def maxlenf(self):
    #     return len(max(self.samples, key=len))


if __name__ == '__main__':
    dataset = FinBotDataset('../input/Ask', 519)
    print(len(dataset))
    print(dataset[100])
    print(dataset[122:130])
# text = []

# filepath = '../input/Ask'

# with open(filepath) as fp:
#     line = fp.readline()
#     while line:
#         line = fp.readline()
#         text.append(line)

# print('len text {}'.format(len(text)))

# chars = set(''.join(text))

# int2char = dict(enumerate(chars))

# char2int = {char: ind for ind, char in int2char.items()}

# maxlen = len(max(text, key=len))
# print("The longest string has {} characters".format(maxlen))

# for i in range(len(text)):
#     while len(text[i]) < maxlen:
#         text[i] += ' '


# input_seq = []
# target_seq = []

# for i in range(len(text)):
#     # Remove last character for input sequence
#     input_seq.append(text[i][:-1])

#     # Remove firsts character for target sequence
#     target_seq.append(text[i][1:])
#     #print("Input Sequence: {}\nTarget Sequence: {}".format(input_seq[i], target_seq[i]))


# for i in range(len(text)):
#     input_seq[i] = [char2int[character] for character in input_seq[i]]
#     target_seq[i] = [char2int[character] for character in target_seq[i]]


# dict_size = len(char2int)
# seq_len = maxlen - 1
# batch_size = len(text)


# def one_hot_encode(sequence, dict_size, seq_len, batch_size):
#     # Creating a multi-dimensional array of zeros with the desired output shape
#     features = np.zeros((batch_size, seq_len, dict_size), dtype=np.float32)

#     # Replacing the 0 at the relevant character index with a 1 to represent that character
#     for i in range(batch_size):
#         for u in range(seq_len):
#             features[i, u, sequence[i][u]] = 1
#     return features


# input_seq = one_hot_encode(input_seq, dict_size, seq_len, batch_size)
# #print("Input shape: {} --> (Batch Size, Sequence Length, One-Hot Encoding Size)".format(input_seq.shape))


# input_seq = torch.from_numpy(input_seq)
# target_seq = torch.Tensor(target_seq)


# class Model(nn.Module):
#     def __init__(self, input_size, output_size, hidden_dim, n_layers):
#         super(Model, self).__init__()

#         # Defining some parameters
#         self.hidden_dim = hidden_dim
#         self.n_layers = n_layers

#         # Defining the layers
#         # RNN Layer
#         self.rnn = nn.RNN(input_size, hidden_dim, n_layers, batch_first=True)
#         # Fully connected layer
#         self.fc = nn.Linear(hidden_dim, output_size)

#     def forward(self, x):

#         batch_size = x.size(0)

#         # Initializing hidden state for first input using method defined below
#         hidden = self.init_hidden(batch_size)

#         # Passing in the input and hidden state into the model and obtaining outputs
#         out, hidden = self.rnn(x, hidden)

#         # Reshaping the outputs such that it can be fit into the fully connected layer
#         out = out.contiguous().view(-1, self.hidden_dim)
#         out = self.fc(out)

#         return out, hidden

#     def init_hidden(self, batch_size):
#         # This method generates the first hidden state of zeros which we'll use in the forward pass
#         hidden = torch.zeros(self.n_layers, batch_size,
#                              self.hidden_dim).to(device)
#         # We'll send the tensor holding the hidden state to the device we specified earlier as well
#         return hidden


# model = Model(input_size=dict_size, output_size=dict_size,
#               hidden_dim=12, n_layers=1)
# # We'll also set the model to the device that we defined earlier (default is CPU)
# model.to(device)

# # Define hyperparameters
# n_epochs = 10000
# lr = 0.01

# # Define Loss, Optimizer
# criterion = nn.CrossEntropyLoss()
# optimizer = torch.optim.Adam(model.parameters(), lr=lr)

# for epoch in range(1, n_epochs + 1):
#     optimizer.zero_grad()  # Clears existing gradients from previous epoch
#     input_seq = input_seq.to(device)
#     output, hidden = model(input_seq)
#     loss = criterion(output.to(device), target_seq.to(device).view(-1).long())
#     loss.backward()  # Does backpropagation and calculates gradients
#     optimizer.step()  # Updates the weights accordingly

#     if epoch % 200 == 0:
#         print('Epoch: {}/{}.............'.format(epoch, n_epochs), end=' ')
#         print("Loss: {:.4f}".format(loss.item()))


# def predict(model, character):
#     # One-hot encoding our input to fit into the model
#     character = np.array([[char2int[c] for c in character]])
#     character = one_hot_encode(character, dict_size, character.shape[1], 1)
#     character = torch.from_numpy(character)
#     character = character.to(device)

#     out, hidden = model(character)

#     prob = nn.functional.softmax(out[-1], dim=0).data
#     # Taking the class with the highest probability score from the output
#     char_ind = torch.max(prob, dim=0)[1].item()

#     return int2char[char_ind], hidden


# def sample(model, out_len, start='hey'):
#     model.eval()  # eval mode
#     start = start.lower()
#     # First off, run through the starting characters
#     chars = [ch for ch in start]
#     size = out_len - len(chars)
#     # Now pass in the previous characters and get a new one
#     for ii in range(size):
#         char, h = predict(model, chars)
#         chars.append(char)

#     return ''.join(chars)


# print(sample(model, 25, 'soita'))
