
import sys
sys.path.insert(0, "../input/libs")
from datasets import FinBotDataset
import torch
from torch import nn
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
import numpy as np
import sys


dataset = FinBotDataset('../input/Ask', 530)
print(len(dataset))
# print(dataset[100])

is_cuda = torch.cuda.is_available()

if is_cuda:
    device = torch.device("cuda")
    print("GPU is available")
else:
    device = torch.device("cpu")
    print("GPU not available, CPU used")



class Model(nn.Module):
    def __init__(self, input_size, output_size, hidden_dim, n_layers):
        super(Model, self).__init__()

        # Defining some parameters
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers

        # Defining the layers
        # RNN Layer
        self.rnn = nn.RNN(input_size, hidden_dim, n_layers, batch_first=True)
        # Fully connected layer
        self.fc = nn.Linear(hidden_dim, output_size)

    def forward(self, x):

        batch_size = x.size(0)

        # Initializing hidden state for first input using method defined below
        hidden = self.init_hidden(batch_size)

        # Passing in the input and hidden state into the model and obtaining outputs
        out, hidden = self.rnn(x, hidden)

        # Reshaping the outputs such that it can be fit into the fully connected layer
        out = out.contiguous().view(-1, self.hidden_dim)
        out = self.fc(out)

        return out, hidden

    def init_hidden(self, batch_size):
        # This method generates the first hidden state of zeros which we'll use in the forward pass
        hidden = torch.zeros(self.n_layers, batch_size,
                             self.hidden_dim).to(device)
        # We'll send the tensor holding the hidden state to the device we specified earlier as well
        return hidden


dict_size = len(dataset.char2int)

model = Model(input_size=dict_size, output_size=dict_size,
              hidden_dim=12, n_layers=1)
model.to(device)

n_epochs = 20
lr = 0.01
batch_size = 64
# Define Loss, Optimizer
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=lr)
dataloader = DataLoader(dataset, batch_size=batch_size,
                        shuffle=True, drop_last=True,num_workers=2)

for epoch in range(n_epochs):
    for batch_i, samples in enumerate(dataloader):

        optimizer.zero_grad()
        input_seq, target_seq = samples
        input_seq = input_seq.to(device)
        output, hidden = model(input_seq)
        loss = criterion(output.to(device),
                         target_seq.to(device).view(-1).long())
        loss.backward()
        optimizer.step()
        if epoch % 10 == 0:
            print('Epoch: {}/{}.............'.format(epoch, n_epochs), end=' ')
            print("Loss: {:.4f}".format(loss.item()))


def predict(model, character):
    # One-hot encoding our input to fit into the model
    character = np.array([[dataset.char2int[c] for c in character]])
    character = dataset.one_hot_encode(
        character, dict_size, character.shape[1], 1)
    character = torch.from_numpy(character)
    character = character.to(device)

    out, hidden = model(character)

    prob = nn.functional.softmax(out[-1], dim=0).data
    # Taking the class with the highest probability score from the output
    char_ind = torch.max(prob, dim=0)[1].item()

    return dataset.int2char[char_ind], hidden


def sample(model, out_len, start='hey'):
    model.eval()  # eval mode
    start = start.lower()
    # First off, run through the starting characters
    chars = [ch for ch in start]
    size = out_len - len(chars)
    # Now pass in the previous characters and get a new one
    for ii in range(size):
        char, h = predict(model, chars)
        chars.append(char)

    return ''.join(chars)


print(sample(model, 25, 'soita'))
